#!/usr/bin/env python3

from graphviz import Digraph
from queue import Queue

marking_color = "#00AA00"
standard_color = "black"

gg = Digraph("g")
num_g = 0

class FlowEdge:
    def __init__(self, source, capacity, target, isAntiEdge=False):
        self.source = source
        self.capacity = capacity
        self.target = target
        self.isAntiEdge = isAntiEdge
        self.flow = 0
    
    def __eq__(self, other):
        if not isinstance(other, FlowEdge):
            return False

        return self.source == other.source and self.target == other.target

    def __hash__(self):
        return hash((self.source, self.target))

    def __repr__(self):
        return str((self.source, self.target)) + "[" + str(self.flow) + "/" + str(self.capacity) + "]"    

def getAugmentingPath(g, source, target):
    q = Queue()
    q.put(source)
    par = {source: None}
    vis = [False for _ in range(len(g))]
    vis[source] = True
    while not q.empty():
        u = q.get()
        vis[u] = True
        for v in g[u]:
            if not vis[v] and g[u][v].flow > 0:
                par[v] = u
                if v == target:
                    path = [v]
                    curr = v
                    while(curr != source):
                        curr = par[curr]
                        path.insert(0, curr)

                    return tuple(path)
                else:
                    q.put(v)
    return None

def fordfulkerson(fn, source, target):
    rn = [{} for _ in range(len(fn))]

    # Initialize residual network
    for u in range(len(fn)):
        for v in fn[u]:
            e = fn[u][v]

            fe = FlowEdge(u, e.capacity, v)
            fe.flow = e.capacity 
            rn[u][v] = fe
            
            afe = FlowEdge(v, e.capacity, u)
            afe.flow = 0
            afe.isAntiEdge = True
            rn[v][u] = afe
    
    # print("Flow network", fn)
    # print("Residual network", rn)

    max_flow = 0

    while(True):
        path = getAugmentingPath(rn, source, target)

        if path == None:
            break

        # print("Augmenting path", str(path))

        extra_flow = min([rn[path[i]][path[i+1]].capacity for i in range(len(path) - 1)])
        # print("Extra flow", extra_flow)
        max_flow += extra_flow

        for i in range(len(path) - 1):
            u = path[i]
            v = path[i+1]

            rn[u][v].flow -= extra_flow
            rn[v][u].flow += extra_flow

            if rn[u][v].isAntiEdge:
                fn[v][u].flow -= extra_flow
            else:
                fn[u][v].flow += extra_flow
        
        # print("Flow network", fn)
        # print("Residual network", rn)

        addGraphToOut(fn, rn, source, target, path)
    
    # print("Flow network", fn)
    # print("Residual network", rn)

    addGraphToOut(fn, rn, source, target, (), final=True)
    printGraph()

    return max_flow

def usageNode(g, u, source, target):
    cap_in = 0
    cap_out = 0
    flow_through_u = 0
    for x in range(len(g)):
        for y in g[x]:
            if y == u:
                cap_in += g[x][y].capacity
            elif x == u:
                cap_out += g[x][y].capacity
            if x == u:
                flow_through_u += g[x][y].flow
    
    if u == source:
        effective_capacity = cap_out
    elif u == target:
        effective_capacity = cap_in
    else:
        effective_capacity = min(cap_in, cap_out)
    
    if effective_capacity == 0:
        return 0

    return flow_through_u / effective_capacity

def construct_gv(g, id, source, target, path, label="Dummy Label", flow=False):
    global marking_color
    global standard_color

    dot = Digraph(id, graph_attr={'label': label})

    nodes_to_color = set(path)
    edges_to_color = set([(path[i], path[i+1]) for i in range(len(path) - 1)])
    for u in range(len(g)):
        node_color = standard_color
        if flow:
            usage = usageNode(g, u, source, target)
            node_color = "#" + "{:02x}".format(int(255 * usage)) + "00" + "{:02x}".format(int(255 * (1- usage)))
        

        node_color = marking_color if (u in nodes_to_color) and (not flow) else node_color
        dot.node(id + str(u), str(u), color=node_color, fontcolor=node_color)
        for v in g[u]:
            edge = g[u][v]
            toBeColored = (edge.source, edge.target) in edges_to_color
            if (not flow) and (not toBeColored) and edge.flow == 0:
                continue
            if flow:
                usage = edge.flow / edge.capacity
                edge_color = "#" + "{:02x}".format(int(255 * usage)) + "00" + "{:02x}".format(int(255 * (1- usage)))
            else:
                edge_color = standard_color
            edge_color = marking_color if (toBeColored and not flow) else edge_color
            dot.edge(id + str(u), id + str(edge.target), label="[" + str(edge.flow) + "/" + str(edge.capacity) + "]", fontcolor=edge_color, color=edge_color)
    
    return dot


def addGraphToOut(fn, rn, source, target, path, final=False):
    global gg
    global num_g

    g_id = "cluster_" + str(num_g)
    step_graph = Digraph(g_id, node_attr={'shape': 'circle'}, graph_attr={'label': 'Result' if final else 'Step ' + str(num_g + 1)})

    rn_graph = construct_gv(rn, g_id + "_rn", source, target, path, label="Residual network")
    step_graph.subgraph(graph=rn_graph)

    fn_graph = construct_gv(fn, g_id + "_fn", source, target, path, label="Flow network", flow=True)
    step_graph.subgraph(graph=fn_graph)

    gg.subgraph(graph=step_graph)

    num_g += 1

def printGraph():
    global gg
    gg.render('out/gg.gv', view=True)

def getEdgeFromInput(in_edge, num_vertices):
    in_edge = in_edge.split(' ')
    if len(in_edge) != 3:
        return None
    
    try:
        fr = int(in_edge[0])
        cap = int(in_edge[1])
        to = int(in_edge[2])
    except ValueError:
        return None
    
    accepting_range = range(num_vertices)

    if not (fr in accepting_range and to in accepting_range):
        return None
    
    if cap <= 0:
        return None
    
    return (fr, cap, to)

def main():
    n = 0
    while(True):
        try:
            n = int(input("Numbers of vertices: "))
            break
        except ValueError:
            continue
    g = [{} for _ in range(n)]
    
    print("Type in the following form: <from> <capacity> <to>.")
    print("Enter 'q' to quit.")

    i = 1
    while(True):
        e_in = input("Edge " + str(i) + ": ")
        if e_in == "q":
            break
        
        e = getEdgeFromInput(e_in, n)
        if e == None:
            print("Cannot parse edge. Please try again.")
            continue
        
        fe = FlowEdge(e[0], e[1], e[2])

        if fe.source == fe.target:
            print("Self loops are not allowed.")
            continue
        
        if fe.source in g[fe.target]:
            print("Antiparallel edges are not supported yet.")
            continue

        if fe.target in g[fe.source]:
            print("Edge " + str(g[fe.source][fe.target]) + " will be overridden to " + str(fe) + ".")

        g[fe.source][fe.target] = fe
        i += 1

    source = None
    target = None
    while(True):
        try:
            source = int(input("Source node: "))
        except ValueError:
            print("Please type in an integer.")
            continue
        if not (source in range(n)):
            print("Invalid source node specified. Please try again.")
            continue
        break
    
    while(True):
        try:
            target = int(input("Target/sink node: "))
        except ValueError:
            print("Please type in an integer.")
            continue
        if not (target in range(n)):
            print("Invalid target/sink node specified. Please try again.")
            continue
        break
    
    print("Running FFA on graph...")

    print("Max flow: ", fordfulkerson(g, source, target))

if __name__ == "__main__":
    main()